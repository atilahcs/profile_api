/**
 * Import Controller
 * Purpose is to get whole information from prestashop
 */
const PS_API_PATH = '';
const PS_TOKEN = '';
module.exports = {
    /**
     * Path: /import/profile
     * Method: get
     * Get basic information of profile from prestashop side
     */
    profile: async (req, res)=>{
        try {
            let prestashop_model = {
                token: PS_TOKEN,
                profile: {
                    method: 'imp_prof'
                }
            };
            //get data from prestashop
            const ps_result = await HttpService.requestToWishbeer(prestashop_model, PS_API_PATH);
            const validate_birthday = await checkBirthday(ps_result);
            //create them on profile
            const customer_list = await Account.create(validate_birthday);
            return res.status(200);
        } catch (err) {
            console.error('============================');
            console.error('Controller: Import');
            console.error('Action: Profile (import profile)');
            console.error('Time: ', new Date());
            console.error('Error message: ', ps_result.message);
            console.error('============================');
            return res.json(406, { mmessage: err.mmessage });
        }
    }
}

/**
 * This function is created as to fix bug of datetype from prestashop side
 * Some attributes are coming in like '0000-00-00' which prestashop db is allowed but profile db isn't
 */
function checkBirthday(array_object){
    return new Promise( (resolve, reject) => {
        try {
            let new_array = [];
            for (let i=0;i<array_object.length;i++) {
                let customer = {
                    ps_id: array_object[i].ps_id,
                    gender: array_object[i].gender,
                    firstname: array_object[i].firstname,
                    lastname: array_object[i].lastname,
                    email: array_object[i].email,
                    status: array_object[i].status,
                    how_you_find_us: array_object[i].how_you_find_us
                };
                if (array_object[i].birthday !== '0000-00-00') {
                    customer.birthday = new Date(array_object[i].birthday);
                }
                if (array_object[i].registration_date !== '0000-00-00') {
                    customer.registration_date = new Date(array_object[i].registration_date);
                }
                new_array.push(customer);
            }
            resolve(new_array);
        } catch (err) {
            reject({ message: err.message });
        }
    });
}