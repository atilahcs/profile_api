/**
 * Presta Controller
 * This controller is in charge of taking care data from prestashop side to create/update/delete on profile side
 * Making data on profile side to has the same data as prestashop
 */
let bcrypt = require('bcrypt');
const SALT_ROUNDS = 10;
module.exports = {
    /**
     * Path: /presta/profile
     * Method: post
     * Register new profile from prestashop side
     * Required params:
     * ->id (become ps_id/prestashop id)
     * ->firstname
     * ->lastname
     * ->email
     * ->dwp (password)
     * Additional params:
     * ->gender
     */
    addProfile: async function(req, res){
        //get all params
        const { id, ps_id = id, firstname, lastname, email, dwp, gender = 0 } = req.allParams();

        try {
            //validate required
            const validate_result = await ParamService.validate([ps_id, firstname, lastname, email, dwp]);

            //create profile
            const encrypted_password = await bcrypt.hash(dwp, SALT_ROUNDS);
            const create_profile_result = await Account.create({ ps_id: ps_id, firstname: firstname, lastname: lastname, email: email, password: encrypted_password, gender: gender });
            delete create_profile_result.password;
            delete create_profile_result.ps_id;
            return res.json(true);
        } catch (err) {
            console.error('============================');
            console.error('Controller: Presta');
            console.error('Action: AddProfile');
            console.error('Params:', req.allParams());
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
    /**
     * Path: /presta/profile/:ps_id
     * Method: put
     * Update profile basic information (incliding password)
     * Required params:
     * ->ps_id
     * ->firstname
     * ->lastname
     * ->email
     * Additional params:
     * ->gender (default is 0, 1 is male, 2 is female)
     * ->dwp (password) //on prestashop side, password is optional and updated at the same time as other information
     */
    updateProfile: async function(req, res){
        //get all params
        const { ps_id, firstname, lastname, email, gender = 0, dwp } = req.allParams();
        try {
            //validate required params
            const validate_result = await ParamService.validate([ps_id, firstname, lastname, email]);

            //update profile
            let profile_obj = {
                firstname: firstname,
                lastname: lastname,
                email: email,
                gender: gender
            };
            const encrypted_password = await bcrypt.hash(dwp, SALT_ROUNDS);
            if (dwp)
                profile_obj.password = encrypted_password;
            const updated_profile_result = await Account.update({ ps_id: ps_id }, profile_obj);
            if (updated_profile_result.length === 0)
                throw { message: 'Profile not found' };
            return res.json(true);
        } catch (err) {
            console.error('============================');
            console.error('Controller: Presta');
            console.error('Action: UpdateProfile');
            console.error('Params:', req.allParams());
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
    /**
     * Path: /presta/profile/:ps_id/enable
     * Method: put
     * Enable profile
     * Required params:
     * ->ps_id (prestashop id) //get from path
     */
    enableProfile: async function(req, res){
        //get all params
        const { ps_id } = req.allParams();
        try {
            //validate required params
            const validate_result = await ParamService.validate([ps_id]);

            //update profile by active status become true
            const enabled_result = await Account.update({ ps_id: ps_id }, { status: true });
            if (enabled_result.length === 0)
                throw { message: 'Profile not found' };
            else
                return res.json(true);
        } catch (err) {
            console.error('============================');
            console.error('Controller: Presta');
            console.error('Action: enableProfile');
            console.error('Params:', req.allParams());
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
    /**
     * Path: /presta/profile/:ps_id/disable
     * Metod: put
     * Disable profile
     * Required params:
     * ->ps_id (prestashop id) //get from path
     */
    disableProfile: async function(req, res){
        //get all params
        const { ps_id } = req.allParams();
        try {
            //validate required params
            const validate_result = await ParamService.validate([ps_id]);

            //update profile by active status become false
            const enabled_result = await Account.update({ ps_id: ps_id }, { status: false });
            if (enabled_result.length === 0)
                throw { message: 'Profile not found' };
            else
                return res.json(true);
        } catch (err) {
            console.error('============================');
            console.error('Controller: Presta');
            console.error('Action: disableProfile');
            console.error('Params:', req.allParams());
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
    /**
     * Path: /presta/profile/:ps_id/address
     * Method: post
     * Create new address
     * Required params:
     * ->ps_id
     * ->addr_id (address id, become ps_addr_id)
     * ->alias (name of address)
     * ->firstname
     * ->lastname
     * ->detail (address detail, for example 1491 Sukhumvit 67)
     * ->city
     * ->state (this will be useful as it is able to read on profile side)
     * ->state_id (for prestashop)
     * ->country
     * ->country_id (for prestashop)
     * ->postcode
     * ->phone_mobile (prestashop customer side focus on this one)
     * Additional params:
     * ->phone (in case many phone number)
     */
    addAddress: async function(req, res){
        //get all params
        const { ps_id, addr_id, ps_addr_id = addr_id } = req.allParams();
        const { alias, firstname, lastname, detail, city, state, state_id, country, country_id, postcode, phone, phone_mobile } = req.allParams();
        
        try {
            //validate required params
            const validate_result = await ParamService.validate([ps_id, alias, firstname, lastname, detail, city, state, state_id, country, country_id, postcode, phone_mobile, ps_addr_id]);
            const profile_result = await Account.findOne({ ps_id: ps_id });
            if (!profile_result)
                throw { message: 'Profile not found' };
            
            //create address
            let address_obj = {
                id: addr_id,
                ps_id: ps_addr_id,
                alias: alias,
                firstname: firstname,
                lastname: lastname,
                detail: detail,
                city: city,
                state: state,
                state_id: state_id,
                country: country,
                country_id: country_id,
                postcode: postcode,
                phone: phone_mobile,
                phone_2: phone,
                profile: profile_result.id,
                profile_ps_id: profile_result.ps_id
            };
            const address_result = await Address.create(address_obj);
            return res.json(true);
        } catch (err) {
            console.error('============================');
            console.error('Controller: Presta');
            console.error('Action: AddAddress');
            console.error('Params:', req.allParams());
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
    /**
     * Path: /presta/profile/:ps_id/address/:ps_addr_id
     * Method: put
     * Update address
     * Required params:
     * ->ps_id
     * ->ps_addr_id (prestashop address id)
     * ->alias (name of address)
     * ->firstname
     * ->lastname
     * ->detail (address detail, for example 1491 Sukhumvit 67)
     * ->city
     * ->state (this will be useful as it is able to read on profile side)
     * ->state_id (for prestashop)
     * ->country
     * ->country_id (for prestashop)
     * ->postcode
     * ->phone_mobile (prestashop customer side focus on this one)
     * Additional params:
     * ->phone (in case many phone number)
     */
    updateAddress: async function(req, res){
        //get all params
        const { ps_id, ps_addr_id, old_addr_id, ps_old_addr_id = old_addr_id } = req.allParams();
        const { alias, firstname, lastname, detail, city, state, state_id, country, country_id, postcode, phone, phone_mobile } = req.allParams();

        try {
            //validate required params
            const validate_result = await ParamService.validate([ps_id, ps_addr_id, alias, firstname, lastname, detail, city, state, state_id, country, country_id, postcode, phone_mobile]);

            //check profile exist
            const profile_result = await Account.findOne({ ps_id: ps_id });
            if (!profile_result)
                throw { message: 'Profile not found' };

            //update address
            let address_obj = {
                alias: alias,
                firstname: firstname,
                lastname: lastname,
                detail: detail,
                city: city,
                state: state,
                state_id: state_id,
                country: country,
                country_id: country_id,
                postcode: postcode,
                phone: phone_mobile,
                phone_2: phone
            };
            if (ps_old_addr_id) { //if address is already used in retail order, mark old one as deleted and create new one with same information
                const updated_address = await Address.update({ ps_id: ps_old_addr_id }, { deleted: true });
                address_obj.ps_id = ps_addr_id;
                const created_address = await Address.create(address_obj);
                return res.json(true);
            } else { //just update it in case address never use in any retail order
                const updated_address = await Address.update({ ps_id: ps_addr_id }, address_obj);
                return res.json(true);
            }
        } catch (err) {
            console.error('============================');
            console.error('Controller: Presta');
            console.error('Action: UpdateAddress');
            console.error('Params:', req.allParams());
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
    /**
     * Path: /presta/profile/:ps_id/address/:ps_addr_id
     * Method: delete
     * Delete add address
     * Required params:
     * ->ps_id (profile prestashop id) //get from path
     * ->ps_addr_id (address prestashop id) //get from path
     * Additional params:
     * ->deleted (boolean) //Used by prestashop.
     *                     //When an address is used in retail order, the address will be marked as deleted instead of deleting in order to used displaying in retail order address
     */
    deleteAddress: async function(req, res){
        //get all params
        const { ps_id, ps_addr_id, deleted = false } = req.allParams();
        try {
            //validate required params
            const validate_result = await ParamService.validate([ps_id, ps_addr_id]);

            //check profile exist
            const profile_result = await Account.findOne({ ps_id: ps_id });
            if (!profile_result)
                throw { message: 'Profile not found' };

            //delete address
            if (deleted) { //mark as deleted if already used in retail order
                const address_updated = await Address.update({ ps_id: ps_addr_id }, { deleted: true });
                return res.json(true);
            } else { //just delete it
                const address_deleted = await Address.destroy({ ps_id: ps_addr_id });
                return res.json(true);
            }
        } catch (err) {
            console.error('============================');
            console.error('Controller: Presta');
            console.error('Action: DeleteAddress');
            console.error('Params:', req.allParams());
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    }
}