/**
 * Admin Profile Controller
 * This controller is for POS or Admin app to use
 */
let bcrypt = require('bcrypt');
let SALT_ROUNDS = 10;
const PS_API_PATH = '';
const PS_TOKEN = '';
module.exports = {
    /**
     * Path: /admin/profile
     * Method: post
     * Register an profile on both Profile DB and Prestashop
     * Required params:
     * ->firstname (string)
     * ->lastname (string)
     * ->email (string)
     * ->phone (string)
     * ->source (string) //Use to record that where does it register to a system. For example, Home Bar 67, Profile admin app
     * Additional params:
     * ->gender (integer, 1 as male or 2 as female, default is 0)
     * ->birthday (datetime)
     */
    add: async (req, res)=>{
        //get all params
        const { firstname, lastname, email, phone, gender = 0, birthday, source } = req.allParams();
        try {
            //validate required params
            const validate_result = await ParamService.validate([firstname, lastname, email, phone, source]);

            //generate random password for an account
            //password is required to create on prestashop
            let password = '';
            let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            for (var i = 0; i < 10; i++)
                password += possible.charAt(Math.floor(Math.random() * possible.length));

            // initialization of prestshop model
            var prestashop_model = {
                token: PS_TOKEN,
                profile: {
                    method: 'add',
                    firstname: firstname,
                    lastname: lastname,
                    email: email,
                    password: password,
                    gender: gender,
                    phone: phone,
                    birthday: birthday,
                    howyoufind: 'Profile API - add'
                }
            };

            //create on prestashop first
            const ps_result = await HttpService.requestToWishbeer(prestashop_model, PS_API_PATH);
            if (ps_result.error)
                throw { message: ps_result.message };

            //account creation data in profile
            let profile = {
                ps_id: ps_result.customer.id,
                firstname: firstname,
                lastname: lastname,
                email: email,
                phone: phone,
                birthday: birthday,
                source: source
            };
            const created_profile = await Account.create(profile);
            delete created_profile.password;
            delete created_profile.ps_id;
            return res.json({ profile: [created_profile] });
        } catch (err) {
            console.error('============================');
            console.error('Controller: Admin Profile');
            console.error('Action: Add');
            console.error('Params:', req.allParams());
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
    /**
     * Path: /admin/profile/:id
     * Method: get
     * Get detail or basic information of a profile
     * Required params:
     * ->id //get from path
     */
    getDetail: async (req, res)=>{
        let { id } = req.allParams();
        try {
            const validate_result = await ParamService.validate([id]);
            const profile_result = await Account.find({ select: ['id', 'firstname', 'lastname', 'email', 'phone', 'gender', 'status', 'birthday'], id: id }).where({ status: true });
            return res.json({ profile: profile_result} );
        } catch (err) {
            console.error('============================');
            console.error('Controller: Admin Profile');
            console.error('Action: Get Detail');
            console.error('Params:', req.allParams());
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
    /**
     * Path: /admin/profile
     * Method: get
     * Get list of profile to display or search profiles
     * Additional params:
     * ->firstname (string)
     * ->lastname (string)
     * ->email (string)
     * ->phone (string)
     * //in case no param to search, it will return list of customer instead
     * ->page (default = 1) //Number of page of profile list. Use it for pagination
     * ->limit (default = 50) //Number to limit to display per page
     */
    getList: async (req, res)=>{
        //retrieve param
        let { page = 1, limit = 50, firstname, lastname, email, phone } = req.allParams();
        try {
            //set up attributes for return
            let search_attribute = { select: ['id', 'firstname', 'lastname', 'email', 'phone', 'gender', 'status', 'birthday'] };
            //add attributes to look for
            if (firstname)
                search_attribute.firstname = firstname;
            if (lastname)
                search_attribute.lastname = lastname;
            if (email)
                search_attribute.email = email;
            if (phone)
                search_attribute.phone = phone;
            if (phone) { //if phone, search only in profile db
                const profile_list = await Account.find(search_attribute).where({ status: true });
                return res.json({ profile: profile_list });
            } else if (firstname || lastname || email) { //if no phone, but there's other params to search
                //initialize prestashop model to request
                var prestashop_model = {
                    token: ps_token,
                    profile: {
                        method: 'find'
                    }
                };
                if (firstname)
                    prestashop_model.profile.firstname = firstname;
                if (lastname)
                    prestashop_model.profile.lastname = lastname;
                if (email)
                    prestashop_model.profile.email = email;

                //search in prestashop
                const ps_result = await HttpService.requestToWishbeer(prestashop_model, PS_API_PATH);
                if (ps_result.length == 0)
                    return res.json({ profile: [] });
                var email_array = [];
                //add email to email_array for checking before create
                //also convert number data (id and gender) in string to integer
                for (var i=0;i<ps_result.length;i++) {
                    email_array.push({ email: ps_result[i].email });
                    ps_result[i].birthday = new Date(ps_result[i].birthday);
                    ps_result[i].registration_date = new Date(ps_result[i].registration_date);
                }
                //add prestashop information to profile database if found any new profile to db.
                const find_or_create_results = await Account.findOrCreate(email_array, ps_result);

                //search again on profile db
                const profile_list = await Account.find(search_attribute).where({ status: true });
                return res.json({ profile: profile_list });
            } else {
                const profile_list = await Account.find(search_attribute).paginate({ page: page, limit: limit }).where({ status: true });
                return res.json({ profile: profile_list });
            }
        } catch (err) {
            console.error('============================');
            console.error('Controller: Admin Profile');
            console.error('Action: Get List');
            console.error('Params:', req.allParams());
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
    /**
     * Path: /admin/profile/:id
     * Method: put
     * Update information of a profile
     * Required params:
     * ->id //get from path
     * ->firstname (string)
     * ->lastname (string)
     * ->email (string)
     * ->phone (string)
     * Additional params:
     * ->gender (0, 1 as male, 2 as female, default is 0)
     * ->birthday
     */
    update: async (req, res)=>{
        //get all params
        const { id, firstname, lastname, email, phone, gender = 0, birthday } = req.allParams();
        try {
            //validate required
            const validate_result = await ParamService.validate([id, firstname, lastname, email, phone]);

            //initialize profile object for update
            let account_obj = {
                firstname: firstname,
                lastname: lastname,
                email: email,
                phone: phone
            };
            gender ? account_obj.gender : gender;
            birthday ? account_obj.birthday : birthday;
            
            //update profile
            const updated_list = await Account.update({ id: id }, account_obj);
            if (updated_list.length === 0)
                throw { message: 'Profile not found' };
            delete updated_list[0].password;
            delete updated_list[0].prestashop_id;
            
            //initialize prestashop model
            var prestashop_model = {
                token: ps_token,
                profile: {
                    method: 'update',
                    email: updated_result.email,
                    firstname: updated_result.firstname,
                    lastname: updated_result.lastname,
                    id_customer: updated_result.ps_id,
                    gender: updated_result.gender,
                    phone: updated_result.phone
                }
            };

            //send request to update profile on prestashop side
            const ps_result = await HttpService.requestToWishbeer(prestashop_model, PS_API_PATH);
            return res.json({ profile: updated_list });
        } catch (err) {
            console.error('============================');
            console.error('Controller: Admin Profile');
            console.error('Action: update');
            console.error('Params:', req.allParams());
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
    /**
     * Path: /admin/profile/:id/enable
     * Method: put
     * Enable profile or activate profile to be active.
     * On Prestashop side, if a profile is disable or non-active means that unable to use.
     * Required params:
     * ->id //get from path
     */
    enable: async (req, res)=>{
        //get all params
        const { id } = req.allParams();
        try {
            //validate required params
            const validate_result = await ParamService.validate([id]);

            //update/enable profile
            const enable_list = await Account.update({ id: id }, { status: true });
            if (enable_list.length === 0)
                throw { message: 'Profile not found' };
            
            //initialize prestashop model
            var prestashop_model = {
                token: ps_token,
                profile: {
                    method: 'enable',
                    id: enable_list[0].ps_id
                }
            };

            //enable profile on prestashop side
            const enable_result = await HttpService.requestToWishbeer(prestashop_model, PS_API_PATH);
            delete enable_list[0].password;
            delete enable_list[0].ps_id;
            return res.json({ profile: enable_list });
        } catch (err) {
            console.error('============================');
            console.error('Controller: Admin Profile');
            console.error('Action: Enable');
            console.error('Params:', req.allParams());
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
    /**
     * Path: /admin/profile/:id/disable
     * Method: put
     * Enable profile or deactivate profile to be inactive.
     * On Prestashop side, if a profile is disable or non-active means that unable to use.
     * Required params:
     * ->id //get from path
     */
    disable: async (req, res)=>{
        //get all params
        const { id } = req.allParams();
        try {
            //validate required params
            const validate_result = await ParamService.validate([id]);

            //update/disable profile
            const enable_list = await Account.update({ id: id }, { status: false });
            if (enable_list.length === 0)
                throw { message: 'Profile not found' };
            
            //initialize prestashop model
            var prestashop_model = {
                token: ps_token,
                profile: {
                    method: 'disable',
                    id: enable_list[0].ps_id
                }
            };

            //disable profile on prestashop side
            const enable_result = await HttpService.requestToWishbeer(prestashop_model, PS_API_PATH);
            delete enable_list[0].password;
            delete enable_list[0].ps_id;
            return res.json({ profile: enable_list });
        } catch (err) {
            console.error('============================');
            console.error('Controller: Admin Profile');
            console.error('Action: Disable');
            console.error('Params:', req.allParams());
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
}