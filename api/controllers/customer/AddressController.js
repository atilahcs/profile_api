module.exports = {
    add: async function(req, res){
        var all_params = req.allParams();
        try {
            //validate null and required param
            const validate_null = await ParamService.checkNull(all_params);
            const validate_require = await ParamService.checkRequire(['id', 'firstname', 'lastname', 'detail', 'city', 'state', 'state_id', 'country', 'country_id', 'postcode', 'phone'], validate_null);
            const validate_number = await ParamService.checkNumber(['id', 'state_id', 'country_id'], validate_require);

            const profile_result = await Account.findOne({ id: validate_number.id });
            if (!profile_result) {
                console.error('============================');
                console.error('Controller: Customer Address');
                console.error('Action: Add');
                console.error('Error message: User not found');
                console.error('============================');
                return res.json(406, { message: 'User not found' });
            }

            var prestashop_model = {
                token: '',
                profile: {
                    method: 'add_addr',
                    profile_id: profile_result.prestashop_id,
                    firstname: validate_number.firstname,
                    lastname: validate_number.lastname,
                    detail: validate_number.detail,
                    phone_mobile: validate_number.phone,
                    country_id: validate_number.country_id,
                    state_id: validate_number.state_id,
                    postcode: validate_number.postcode,
                    city: validate_number.city
                }
            }
            const ps_result = await HttpService.requestToWishbeer(prestashop_model, '');
            if (ps_result.error) {
                console.error('============================');
                console.error('Controller: Customer Address');
                console.error('Action: Add');
                console.error('Error message: ', ps_result.message);
                console.error('============================');
                return res.json(406, { message: ps_result.message });
            }
            let address = {
                id: ps_result.address.id,
                prestashop_id: ps_result.address.id,
                firstname: ps_result.address.firstname,
                lastname: ps_result.address.lastname,
                alias: ps_result.address.alias,
                detail: ps_result.address.address1,
                city: ps_result.address.city,
                state_id: ps_result.address.id_state,
                state: validate_number.state,
                country_id: ps_result.address.id_country,
                country: validate_number.country,
                postcode: ps_result.address.postcode,
                phone_mobile: ps_result.address.phone_mobile,
                profile: profile_result.id,
                profile_ps_id: ps_result.address.id_customer
            }
            const address_result = await Address.create(address);
            console.log(address_result);
            return res.json({ address: address_result });
        } catch (err) {
            console.error('============================');
            console.error('Controller: Customer Address');
            console.error('Action: Add');
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
    update: async function(req, res){
        var all_params = req.allParams();
        try {
            //validate null and required param
            const validate_null = await ParamService.checkNull(all_params);
            const validate_require = await ParamService.checkRequire(['id', 'address_id', 'alias', 'firstname', 'lastname', 'detail', 'city', 'state', 'state_id', 'country', 'country_id', 'postcode'], validate_null);
            const validate_number = await ParamService.checkNumber(['id', 'address_id', 'state_id', 'country_id'], validate_require);
            if (!validate_number.phone && !validate_number.phone_mobile) {
                console.error('============================');
                console.error('Controller: Customer Address');
                console.error('Action: Update');
                console.error('Error message: Information missing');
                console.error('============================');
                return res.json(406, { message: 'Information missing' });
            }

            const profile_result = await Account.findOne({ id: validate_number.id });
            if (!profile_result) {
                console.error('============================');
                console.error('Controller: Customer Address');
                console.error('Action: Update');
                console.error('Error message: User not found');
                console.error('============================');
                return res.json(406, { message: 'User not found' });
            }
            const address_result = await Address.findOne({ id: validate_number.address_id });
            if (!address_result) {
                console.error('============================');
                console.error('Controller: Customer Address');
                console.error('Action: Update');
                console.error('Error message: Address not found');
                console.error('============================');
                return res.json(406, { message: 'Address not found' });
            }

            var prestashop_model = {
                token: '',
                profile: {
                    method: 'update_addr',
                    profile_id: profile_result.prestashop_id,
                    address_id: address_result.prestashop_id,
                    firstname: validate_number.firstname,
                    lastname: validate_number.lastname,
                    detail: validate_number.detail,
                    country_id: validate_number.country_id,
                    state_id: validate_number.state_id,
                    postcode: validate_number.postcode,
                    city: validate_number.city,
                    alias: validate_number.alias
                }
            }
            if (validate_number.phone)
                prestashop_model.profile.phone = validate_number.phone;
            if (validate_number.phone_mobile)
                prestashop_model.profile.phone_mobile = validate_number.phone_mobile;
            const ps_result = await HttpService.requestToWishbeer(prestashop_model, '');
            if (ps_result.error) {
                console.error('============================');
                console.error('Controller: Customer Address');
                console.error('Action: Update');
                console.error('Error message:', ps_result.message);
                console.error('============================');
                return res.json(406, { message: ps_result.message });
            }
            var address = {
                prestashop_id: ps_result.address.id,
                firstname: ps_result.address.firstname,
                lastname: ps_result.address.lastname,
                alias: ps_result.address.alias,
                detail: ps_result.address.address1,
                city: ps_result.address.city,
                state: validate_number.state,
                state_id: ps_result.address.id_state,
                country: validate_number.country,
                country_id: validate_number.id_country,
                postcode: ps_result.address.postcode,
                phone: ps_result.address.phone,
                phone_mobile: ps_result.address.phone_mobile
            }
            const updated_address_list = await Address.update({ id: validate_number.address_id }, address);
            var updated_address_result = updated_address_list[0];
            return res.json({ address: updated_address_result });
        } catch (err) {
            console.error('============================');
            console.error('Controller: Customer Address');
            console.error('Action: Update');
            console.error('Error message:', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
    delete: async function(req, res){
        var all_params = req.allParams();
        try {
            //validate null and required param
            const validate_null = await ParamService.checkNull(all_params);
            const validate_require = await ParamService.checkRequire(['id', 'address_id'], validate_null);
            const validate_number = await ParamService.checkNumber(['id', 'address_id'], validate_require);

            const profile_result = await Account.findOne({ id: validate_number.id });
            if (!profile_result) {
                console.error('============================');
                console.error('Controller: Customer Address');
                console.error('Action: Delete');
                console.error('Error message: User not found');
                console.error('============================');
                return res.json(406, { message: 'User not found' });
            }
            const address_search_result = await Address.findOne({ id: validate_number.address_id });
            if (!address_search_result) {
                console.error('============================');
                console.error('Controller: Customer Address');
                console.error('Action: Delete');
                console.error('Error message: Address not found');
                console.error('============================');
                return res.json(406, { message: 'Address not found' });
            }

            var prestashop_model = {
                token: '',
                profile: {
                    method: 'delete_addr',
                    profile_id: profile_result.prestashop_id,
                    address_id: address_search_result.prestashop_id
                }
            }
            const ps_result = await HttpService.requestToWishbeer(prestashop_model, '');
            if (ps_result.error) {
                console.error('============================');
                console.error('Controller: Customer Address');
                console.error('Action: Delete');
                console.error('Error message: ', ps_result.message);
                console.error('============================');
                return res.json(406, { message: ps_result.message });
            }
            const deleted_address_result = await Address.destroy({ id: address_search_result.id });
            if (!deleted_address_result || deleted_address_result.length === 0) {
                console.error('============================');
                console.error('Controller: Customer Address');
                console.error('Action: Delete');
                console.error('Error message: Address not found to delete');
                console.error('============================');
                return res.json(406, { message: 'Address not found to delete' });
            }
            return res.json({ result: true });
        } catch (err) {
            console.error('============================');
            console.error('Controller: Customer Address');
            console.error('Action: Delete');
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    }
}