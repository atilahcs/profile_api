/**
 * Customer Order Bar Controller
 * Display order, connect to Home Bar POS when need detail
 */
module.exports = {
    /**
     * Path: /profile/:id/order_Bar/:order_id
     * Method: get
     * Get detail of an bar order
     * Required params:
     * ->id //get from path, profile id
     * ->order_id (integer) //get from path
     */
    getDetail: async function(req, res){
        //get all params
        const { id, order_id } = req.allParams();
        try {
            //validate required
            const validate_result = await ParamService.validate([id, order_id]);

            //check profile exist
            const profile_result = await Account.findOne({ id: id });
            if (!profile_result)
                throw { message: 'Profile not found' };
            
            //retrieve order detail from POS
            //still request from staging
            const order_request_result = await HttpService.requestBarOrderDetail({ profile_id: id, order_id: order_id });
            let order_bar_detail_result = order_request_result.data;

            //add detail from profile side
            const order_bar_result = await Order_bar.findOne({ id: order_id });
            order_bar_detail_result.profile_id = order_bar_result.profile;
            order_bar_detail_result.profile_name = order_bar_result.profile_name;
            order_bar_detail_result.location = order_bar_result.location;
            order_bar_detail_result.amount = order_bar_result.amount;
            order_bar_detail_result.branch_id = order_bar_result.branch_id;
            
            return res.json({ order: order_bar_detail_result });
        } catch (err) {
            console.error('============================');
            console.error('Controller: Customer Orderbar');
            console.error('Action: Get Detail');
            console.error('Params:', req.allParams());
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
    /**
     * Path: /profile/:id/order_bar
     * Method: get
     * Add new bar order to profile
     * Required params:
     * ->id //get from path, profile id
     * Additional params:
     * ->page (integer, default as 1) //number of page for pagination
     * -> limit (integer, default as 50) //limit a list of order for pagination
     * ->sort (string, DESC and ASC, default as DESC) //use for how you sorting order bar list
     */
    getList: async (req, res)=>{
        //get all params
        const { id, page = 1, limit = 50, sort = 'DESC' } = req.allParams();
        try {
            //validate required params
            const validate_result = await ParamService.validate([id]);

            //check profile exist
            const profile_result = await Account.findOne({ id: id });
            if (!profile_result)
                throw { message: 'Profile not found' };

            //get order bar list
            //since now there's no order list in db, display from ta's api first
            const order_request_result = await HttpService.requestBarOrder({ profile_id: id });
            let order_bar_list = order_request_result.data;
            //need to ask for order detail from ta's api, pos
            return res.json({ order: order_bar_list });
            //actually, list should coming from code's below
            // let sort_txt = 'id '+sort;
            // const order_bar_list = await Order_bar.find({ profile: id }).sort(sort_txt).paginate({ page: page, limit: limit });
            // return res.json({ order_bar_list: order_bar_list });
        } catch (err) {
            console.error('============================');
            console.error('Controller: Customer Orderbar');
            console.error('Action: Get List');
            console.error('Params:', req.allParams());
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    }
}