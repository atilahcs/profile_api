/**
 * Customer Profile Controller
 * This controller is for POS or Admin app to use
 */
var bcrypt = require('bcrypt');
var SALT_ROUNDS = 10;
const PS_API_PATH = '';
const PS_TOKEN = '';
module.exports = {
    /**
     * Path: /profile
     * Method: post
     * Customer register a profile
     * Required params:
     * ->email (string)
     * ->password (string, at least 8 characters) 
     * ->firstname
     * ->lastname
     * Additional params:
     * ->gender (default is 0, 1 is male, 2 is female)
     */
    add: async (req, res)=>{
        //get all params
        const { firstname, lastname, email, password, gender = 0 } = req.allParams();
        try {
            //validate required params
            const validate_result = await ParamService.validate([firstname, lastname, email, password]);
            //check minimum characters of password
            if (password.length < 8)
                throw { message: 'Password must be at least 8 characters' };

            // initialization of prestshop model
            var prestashop_model = {
                token: ps_token,
                profile: {
                    method: 'add',
                    firstname: firstname,
                    lastname: lastname,
                    email: email,
                    password: password,
                    gender: gender,
                    howyoufind: 'Profile API - add'
                }
            };

            //register on prestashop side
            const ps_result = await HttpService.requestToWishbeer(prestashop_model, PS_API_PATH);
            if (ps_result.error)
                throw { message: ps_result.message };

            const encrypted_password = await bcrypt.hash(password, SALT_ROUNDS);
            password = encrypted_password;

            //create customer account
            const create_profile = await Account.create({ ps_id: ps_result.customer.id, firstname: firstname, lastname: lastname, email: email, password: password, gender: gender, source: 'Profile website' });
            delete create_profile.password;
            delete create_profile.ps_id;
            return res.json({ profile: create_profile });
        } catch (err) {
            console.error('============================');
            console.error('Controller: Customer Profile');
            console.error('Action: Add');
            console.error('Params:', req.allParams());
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
    /**
     * Path: /profile/:id
     * Method: get
     * Retrieve basic information of profile
     * Required params:
     * ->id
     */
    get: async (req, res)=>{
        //get all params
        const { id } = req.allParams();
        try {
            //validate required params
            const validate_result = await ParamService.validate([id]);

            //check profile exist
            const profile_result = await Account.findOne({ id: id, select: ['id', 'firstname', 'lastname', 'email', 'phone', 'gender', 'status', 'birthday'] });
            if (!profile_result)
                throw { message: 'Profile not found' };
            if (!profile_result.status)
                throw { message: 'There\'s something wrong with your account. Please contact us for your support' };
            return res.json({ profile: profile_result });
        } catch (err) {
            console.error('============================');
            console.error('Controller: Customer Profile');
            console.error('Action: Get');
            console.error('Params:', req.allParams());
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
    /**
     * Path: /profile/:id
     * Method: put
     * Update basic information of a profile
     * Required params:
     * ->id
     * ->firstname
     * ->lastname
     * ->email
     * Additional params:
     * ->gender (default is 0, 1 is male, 2 is female)
     * ->birthday
     */
    update: async (req, res)=>{
        //get all params
        const { id, firstname, lastname, email, gender = 0, birthday } = req.allParams();
        try {
            //validate required params
            const validate_result = await ParamService.validate([id, firstname, lastname, email]);

            //initialize profile object
            let account_obj = {
                firstname: firstname,
                lastname: lastname,
                email: email,
                gender: gender
            };
            if (birthday)
                account_obj.birthday = birthday;
            //update information on profile db
            const updated_profile_list = await Account.update({ id: id }, account_obj);
            if (updated_profile_list === 0)
                throw { message: 'Profile not found' };
            let updated_profile = updated_profile_list[0];

            //initialize prestashop model
            let prestashop_model = {
                token: ps_token,
                profile: {
                    method: 'update',
                    email: email,
                    firstname: firstname,
                    lastname: lastname,
                    id_customer: updated_profile.ps_id,
                    gender: gender
                }
            }

            //update on prestashop side
            const ps_result = await HttpService.requestToWishbeer(prestashop_model, PS_API_PATH);
            if (ps_result.error)
                throw { message: ps_result.message };
            delete updated_profile.password;
            delete updated_profile.ps_id;
            return res.json({ profile: updated_profile });
        } catch (err) {
            console.error('============================');
            console.error('Controller: Customer Profile');
            console.error('Action: Update');
            console.error('Params:', req.allParams());
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
    /**
     * Path: /profile/:id/password
     * Method: put
     * Update password of a profile
     * Required params:
     * ->current_password
     * ->new_password
     * ->confirm_password
     */
    updatePassword: async (req, res)=>{
        //get all params
        const { id, current_password, new_password, confirm_password } = req.allParams();
        try {
            //validate required params
            const validate_result = await ParamService.validate([id, current_password, new_password, confirm_password]);
            if (new_password.length < 8)
                throw { message: 'Password must be at least 8 characters' };
            if (new_password !== confirm_password)
                throw { message: 'New password and confirmation password are not matched' };

            //check profile exist
            const profile_result = await Account.findOne({ id: id });
            if (!profile_result)
                throw { message: 'Profile not found' };
            
            //compare password
            const compared_result = await bcrypt.compare(current_password, profile_result.password);
            if (!compared_result)
                throw { message: 'Your current password is incorrect' };
            
            //initialize prestashop model
            let prestashop_model = {
                token: ps_token,
                profile: {
                    method: 'update_pwd',
                    id_customer: profile_result.ps_id,
                    email: profile_result.email,
                    old_pwd: current_password,
                    new_pwd: new_password,
                    confirm_pwd: confirm_password,
                }
            };

            //send to update password on prestashop side
            const ps_result = await HttpService.requestToWishbeer(prestashop_model, PS_API_PATH);
            if (ps_result.error)
                throw { message: ps_result.message };
            
            const encrypted_password = await bcrypt.hash(new_password, SALT_ROUNDS);
            const updated_profile_list = await Account.update({ id: id }, { password: encrypted_password });
            var updated_profile_result = updated_profile_list[0];
            delete updated_profile_result.password;
            delete updated_profile_result.ps_id;
            return res.json({ profile: updated_profile_result });
        } catch (err) {
            console.error('============================');
            console.error('Controller: Customer Profile');
            console.error('Action: UpdatePassword');
            console.error('Params:', req.allParams());
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
    /**
     * Path: /profile/authenticate
     * Method: post
     * Request profile authentication
     * 
     * Required params:
     * ->email
     * ->password
     */
    login: async (req, res)=>{
        //get all params
        const { email, password } = req.allParams();
        try {
            //validate required params
            const valdiate_result = await ParamService.validate([email, password]);

            //check profile exist
            const profile_result = await Account.findOne({ email: email });
            if (!profile_result)
                throw { message: 'Email/password is incorrect' };
            if (!profile_result.status)
                throw { message: 'There\'s something wrong with your account. Please contact us for your support' };


            //compare password
            const compared_result = await bcrypt.compare(password, profile_result.password);
            if (compared_result) {
                return res.json({ profile: profile_result });
            }
            delete profile_result.ps_id;
            delete profile_result.password;

            //request login by checking with prestashop side
            let prestashop_model = {
                token: ps_token,
                profile: {
                    method: 'login',
                    email: email,
                    password: password
                }
            };
            const ps_result = await HttpService.requestToWishbeer(prestashop_model, PS_API_PATH);
            if (ps_result.error)
                throw { message: 'Email/password is incorrect' };
            const encrypted_password = await bcrypt.hash(password, SALT_ROUNDS);
            const updated_profile_list = await Account.update({ email: email }, { password: encrypted_password });
            return res.json({ profile: profile_result });
        } catch (err) {
            console.error('============================');
            console.error('Controller: Customer Profile');
            console.error('Action: Login');
            console.error('Params:', req.allParams());
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
    //below use for forget-password
    /**
     * Path: /profile/:email/forget-password
     * Method: get
     * Request to reset password
     * Required params:
     * ->email
     */
    checkEmail: async(req, res)=>{
        //get all params
        const { email } = req.allParams();
        try {
            //validate required
            const validate_result = await ParamService.validate([email]);
            //check profile exist
            const profile_result = await Account.findOne({ email: email });
            if (!profile_result)
                throw { message: 'Profile not found' };
            return res.json(true);
        } catch (err) {
            console.error('============================');
            console.error('Controller: Customer Profile');
            console.error('Action: Check Email');
            console.error('Params:', req.allParams());
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
    /**
     * Path: /profile/:email/forget-password
     * Method: put
     * Reset password after make a request
     * Required params:
     * ->email
     * ->new_password
     * ->confirm_password
     */
    resetPassword: async(req, res)=>{
        //get all params
        const { email, new_password, confirm_password } = req.allParams();
        try {
            //validate quired
            const validate_result = await ParamService.validate([email, new_password, confirm_password]);

            //check profile exist
            const profile_result = await Account.findOne({ email: email });
            if (!profile_result)
                throw { message: 'Profile not found' };
            if (new_password.length < 8)
                throw { message: 'Password must be at least 8 characters' };
            if (new_password !== confirm_password)
                throw { message: 'New password and confirmation password are not matched' };

            //reset password on prestashop side
            let prestashop_model = {
                token: PS_TOKEN,
                profile: {
                    method: 'reset_pwd',
                    id_customer: profile_result.ps_id,
                    new_pwd: new_password,
                    confirm_pwd: confirm_password,
                }
            };
            const ps_result = await HttpService.requestToWishbeer(prestashop_model, PS_API_PATH);
            if (ps_result.error)
                throw { message: ps_result.message };
            
            //reset password on profile side
            const encrypted_password = await bcrypt.hash(new_password, SALT_ROUNDS);
            const updated_profile_list = await Account.update({ email: email }, { password: encrypted_password });
            return res.json(true);
        } catch (err) {
            console.error('============================');
            console.error('Controller: Customer Profile');
            console.error('Action: ResetPassword');
            console.error('Params:', req.allParams());
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    }
}