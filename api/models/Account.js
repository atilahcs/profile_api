module.exports = {
    tableName: 'account',
    attributes: {
        firstname: {
            type: 'string',
            columnName: 'firstname'
        },
        lastname: {
            type: 'string',
            columnName: 'lastname'
        },
        email: {
            type: 'string',
            columnName: 'email',
            unique: true
        },
        password: {
            type: 'string',
            columnName: 'password',
            defaultsTo: ''
        },
        phone: {
            type: 'string',
            columnName: 'phone',
            defaultsTo: '-'
        },
        gender: {
            type: 'integer',
            columnName: 'gender',
            defaultsTo: 0
        },
        ps_id: {
            type: 'integer',
            columnName: 'prestashop_id',
            unique: true
        },
        status: {
            type: 'boolean',
            columnName: 'active_status',
            defaultsTo: true
        },
        birthday: {
            type: 'datetime',
            columnName: 'birthday'
        },
        how_you_find_us:{
            type: 'string',
            columnName: 'how_you_find_us',
            defaultsTo: ''
        },
        source:{
            type: 'string',
            columnName: 'source',
            defaultsTo: ''
        },
        registration_date:{
            type: 'datetime',
            columnName: 'registration_date',
            defaultsTo: new Date()
        },
        addresses: {
            collection: 'address',
            via: 'profile'
        }
    }
}