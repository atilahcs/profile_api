module.exports = {
    tableName: 'address',
    attributes: {
        ps_id: {
            type: 'integer',
            columnName: 'prestashop_id'
        },
        alias: {
            type: 'string',
            columnName: 'alias'
        },
        firstname: {
            type: 'string',
            columnName: 'firstname'
        },
        lastname: {
            type: 'string',
            columnName: 'lastname'
        },
        detail: {
            type: 'text',
            columnName: 'address_detail'
        },
        city: {
            type: 'string',
            columnName: 'city'
        },
        state: {
            type: 'string',
            columnName: 'state_or_province'
        },
        state_id: { //for prestashop
            type: 'integer',
            columnName: 'state_id'
        },
        country: {
            type: 'string',
            columnName: 'country'
        },
        country_id: { //for prestashop
            type: 'integer',
            columnName: 'country_id'
        },
        postcode: {
            type: 'string',
            columnName: 'postcode'
        },
        phone: { //main one
            type: 'string',
            columnName: 'phone',
            defaultsTo: ''
        },
        phone_2: {
            type: 'string',
            columnName: 'phone_2',
            defaultsTo: ''
        },
        profile: {
            model: 'account',
            columnName: 'profile_id'
        },
        profile_ps_id: {
            type: 'integer',
            columnName: 'profile_prestashop_id'
        },
        deleted: {
            type: 'boolean',
            columnName: 'deleted',
            defaultsTo: false
        }
    }
}