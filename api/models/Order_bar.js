module.exports = {
    tableName: 'order_bar',
    attributes: {
        profile: {
            model: 'Account',
            columnName: 'profile_id'
        },
        profile_name: {
            type: 'string',
            columnName: 'profile_name'
        },
        order_id: {
            type: 'integer',
            columnName: 'order_id'
        },
        location: {
            type: 'string',
            columnName: 'order_location'
        },
        branch_id: {
            type: 'integer',
            columnName: 'branch_id'
        },
        amount: {
            type: 'float',
            columnName: 'amount'
        }
    }
}