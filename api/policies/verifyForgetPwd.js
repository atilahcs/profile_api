const SECRET = '';
let jwt = require('jsonwebtoken');

module.exports = async (req, res, next)=>{
    console.log('ip:', req.ip);
    let current_time = new Date();
    console.log('datetime: ', current_time);
    console.log('path:', req.path);
    console.log('========================');
    if (req.headers.token) {
        try {
            const data = await jwt.verify(req.headers.token, SECRET);
            if (data.message !== 'Now I\'m all messed up')
                throw false;
            return next();
        } catch (err) {
            return res.json(401, { message: 'Unauthorized access!' });
        }
    } else
        return res.json(401, { message: 'Unauthorized access!' });
}