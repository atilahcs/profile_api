const PS_SECRET = '';
module.exports = function(req, res, next) {
    console.log('Request from PS');
    console.log('ip:', req.ip);
    let current_time = new Date();
    console.log('datetime: ', current_time);
    console.log('path:', req.path);
    console.log('========================');
    if (req.headers.ps_token && req.headers.ps_token === PS_SECRET)
        return next();
    else
        return res.json(401, { message: 'Unauthorized access!' });
}