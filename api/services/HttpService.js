module.exports = {
    requestToWishbeer: function(data, uripath){
		return new Promise((resolve, reject) => {
			var request = require('request');
			request({
				url: ''+uripath,
				method: 'post',
				json: true,
				body: data
			}, (err, response, body) => {
				if (err)
					reject({ message: 'Some data is invalid' });
				else
					resolve(body);
			})
		});
	},
	requestBarOrder: function(data){
		return new Promise((resolve, reject) => {
			var request = require('request');
			request({
				url: '/profile/'+data.profile_id+'/order',
				method: 'get',
				json: true,
			}, (err, response, body) => {
				if (err)
					reject({ message: 'Some data is invalid' });
				else
					resolve(body);
			})
		});
	},
	requestBarOrderDetail: function(data){
		return new Promise((resolve, reject) => {
			var request = require('request');
			request({
				url: '/profile/'+data.profile_id+'/order/'+data.order_id,
				method: 'get',
				json: true,
			}, (err, response, body) => {
				if (err)
					reject({ message: 'Some data is invalid' });
				else
					resolve(body);
			})
		});
	}
}