module.exports = {
    checkNull: (params) => {
        return new Promise( (resolve, reject) => {
            if (typeof params === 'object') {
                let all_params = new Object(params);
                try {
                    let count = 0;
                    for (var key in all_params)
                        (!all_params[key] || all_params[key] === 'null') ? delete all_params[key] : (all_params[key],count++);
                    
                    if (count)
                        resolve(all_params);
                    else
                        reject({ message: 'Parameter not found' });
                } catch (err) {
                    reject({ message: 'Parameter not found' });
                }
            } else {
                if (params)
                    resolve(params);
                else
                    reject({ message: 'Parameter not found' });
            }
        });
    },
    checkRequire: (keys, params) => {
        return new Promise((resolve, reject) => {
            let all_params = new Object(params);
            keys.forEach(key => {
                if (!all_params[key]) {
                    reject({ message: 'Information missing' });
                }
            });
            resolve(all_params);
        });
    },
    checkNumber: (keys, params) => {
        return new Promise((resolve, reject) => {
            let all_params = new Object(params);
            keys.forEach(key => {
                if (all_params[key]) {
                    all_params[key] = Number(all_params[key]);
                    if (Number.isNaN(all_params[key])) {
                        reject({ message: 'Wrong type of information' });
                    }
                }
            });
            resolve(all_params);
        });
    },
    validate: (array_params) => {
        return new Promise( (resolve, reject) => {
            try {
                array_params.forEach((param)=>{
                    (!param || param === 'null') ? reject({ message: 'Information missing' }) : param;
                });
                resolve(true);
            } catch (err) {
                reject({ message: err.message });
            }
        });
    }
}