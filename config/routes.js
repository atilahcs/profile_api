/**
 * Profile API Routes
 * 
 * Structure ordering
 * ->Admin
 * --->post
 * ------>address
 * ------>order
 * ------>profile
 * --->put
 * --->get
 * --->delete
 * ->Customer
 * ->Presta (Prestashop to Profile API)
 * ->Import
 */
module.exports.routes = {
  //admin
      //post
          //address
          'post /admin/profile/:id/address':{
            controller: 'admin/AddressController',
            action: 'add'
          },
          //order
          'post /admin/profile/:id/order':{
            controller: 'admin/OrderBarController',
            action: 'add'
          },
          //profile
          'post /admin/profile':{
            controller: 'admin/ProfileController',
            action: 'add'
          },
      //put
          //address
          'put /admin/profile/:id/address/:address_id':{
            controller: 'admin/AddressController',
            action: 'update'
          },
          //order
          'get /admin/profile/:id/order_bar/:order_id':{
            controller: 'admin/OrderBarController',
            action: 'update'
          },
          //profile
          'put /admin/profile/:id/enable':{
            controller: 'admin/ProfileController',
            action: 'enable'
          },
          'put /admin/profile/:id/disable':{
            controller: 'admin/ProfileController',
            action: 'disable'
          },
          'put /admin/profile/:id':{
            controller: 'admin/ProfileController',
            action: 'update'
          },
      //get
          //order
          'get /admin/profile/:id/order_bar/:order_id':{
            controller: 'admin/OrderBarController',
            action: 'getDetail'
          },
          'get /admin/profile/:id/order_bar':{
            controller: 'admin/OrderBarController',
            action: 'getDetail'
          },
          //profile
          'get /admin/profile/:id':{
            controller: 'admin/ProfileController',
            action: 'getDetail'
          },
          'get /admin/profile':{
            controller: 'admin/ProfileController',
            action: 'getList'
          },
      //delete
          //address
          'delete /admin/profile/:id/address/:address_id':{
            controller: 'admin/AddressController',
            action: 'delete'
          },

  //customer
      //post
          //address
          'post /profile/:id/address': {
            controller: 'customer/AddressController',
            action: 'add'
          },
          //profile
          'post /profile/authenticate':{
            controller: 'customer/ProfileController',
            action: 'login'
          },
          'post /profile':{
            controller: 'customer/ProfileController',
            action: 'add'
          },
      //put
          //address
          'put /profile/:id/address/:address_id':{
            controller: 'customer/AddressController',
            action: 'update'
          },
          //profile
          'put /profile/:email/forget-password':{
            controller: 'customer/ProfileController',
            action: 'resetPassword'
          },
          'put /profile/:id/password':{
            controller: 'customer/ProfileController',
            action: 'updatePassword'
          },
          'put /profile/:id':{
            controller: 'customer/ProfileController',
            action: 'update'
          },
      //get
          //order
          'get /profile/:id/order_bar/:order_id':{
            controller: 'customer/OrderBarController',
            action: 'getDetail'
          },
          'get /profile/:id/order_bar':{
            controller: 'customer/OrderBarController',
            action: 'getList'
          },
          //profile
          'get /profile/:email/forget-password':{
            controller: 'customer/ProfileController',
            action: 'checkEmail'
          },
          'get /profile/:id':{
            controller: 'customer/ProfileController',
            action: 'get'
          },
      //delete
          //address
          'delete /profile/address': {
            controller: 'customer/AddressController',
            action: 'delete'
          },

  //prestashop
      //post
          //address
          'post /presta/profile/:ps_id/address': {
            controller: 'PrestaController',
            action: 'addAddress'
          },
          //profile
          'post /presta/profile':{
            controller: 'PrestaController',
            action: 'addProfile'
          },
      //put
          //address
          'put /presta/profile/:ps_id/address/:ps_addr_id': {
            controller: 'PrestaController',
            action: 'updateAddress'
          },
          //profile
          'put /presta/profile/:ps_id/enable':{
            controller: 'PrestaController',
            action: 'enableProfile'
          },
          'put /presta/profile/:ps_id/disable':{
            controller: 'PrestaController',
            action: 'disableProfile'
          },
          'put /presta/profile/:ps_id':{
            controller: 'PrestaController',
            action: 'updateProfile'
          },
      //delete
          //address
          'delete /presta/profile/:ps_id/address/:ps_addr_id': {
            controller: 'PrestaController',
            action: 'deleteAddress'
          },

  //import
  'GET /import/profile':{
    controller: 'ImportController',
    action: 'profile'
  },

  //reject other
  '/*': function(req, res){
    return res.json(404);
  }
  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
